class MChild1 extends HTMLElement {
    constructor() {
        super();
        // Shadow Root
        this._root = this.attachShadow({ mode: "open" });
        // Elements
        this._$top = null;
        this._$bottom = null;
        // Data
        this._disabled = false;
        this._value = 0;
        this._touched = false;

        let $template = document.createElement("template");
        $template.innerHTML = `
            <style>
            .c1-container{
                position:absolute;
                top :30px;
                bottom: 30px;
                left:30px;
                right: 30px;
                background: var(--color,pink);
            }
            </style>
            <div class="c1-container">
              <m-child2></m-child2>
            </div>
        `;

        if (window.ShadyCSS) ShadyCSS.prepareTemplate($template, "m-child1");
        this._$template = document.importNode($template.content, true);
    }
    set value(value) {
        if (this._value === value) return;
        this._touched = true;
        this._value = value;
        this._render();
    }
    get value() {
        return this._value;
    }
    connectedCallback() {
        if (window.ShadyCSS) ShadyCSS.styleElement(this);
        this._root.appendChild(this._$template);
   
    }
  
  
}

window.customElements.define("m-child1", MChild1);