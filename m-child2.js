class MChild2 extends HTMLElement {
    constructor() {
        super();
        // Shadow Root
        this._root = this.attachShadow({ mode: "open" });
        // Elements
        this._$top = null;
        this._$bottom = null;
        // Data
        this._disabled = false;
        this._value = 0;
        this._touched = false;

        let $template = document.createElement("template");
        $template.innerHTML = `
            <style>
            .c2-container{
                position:absolute;
                top :20px;
                bottom: 20px;
                left:20px;
                right: 20px;
                background: green;
            }
            </style>
            <div class="c2-container">
              
            </div>
        `;

        if (window.ShadyCSS) ShadyCSS.prepareTemplate($template, "m-child2");
        this._$template = document.importNode($template.content, true);
    }

    connectedCallback() {
        if (window.ShadyCSS) ShadyCSS.styleElement(this);
        this._root.appendChild(this._$template);
       
    }
    
   
}

window.customElements.define("m-child2", MChild2);