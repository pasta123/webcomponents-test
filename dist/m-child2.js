"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MChild2 = function (_HTMLElement) {
    _inherits(MChild2, _HTMLElement);

    function MChild2() {
        _classCallCheck(this, MChild2);

        // Shadow Root
        var _this = _possibleConstructorReturn(this, (MChild2.__proto__ || Object.getPrototypeOf(MChild2)).call(this));

        _this._root = _this.attachShadow({ mode: "open" });
        // Elements
        _this._$top = null;
        _this._$bottom = null;
        // Data
        _this._disabled = false;
        _this._value = 0;
        _this._touched = false;

        var $template = document.createElement("template");
        $template.innerHTML = "\n            <style>\n            .c2-container{\n                position:absolute;\n                top :20px;\n                bottom: 20px;\n                left:20px;\n                right: 20px;\n                background: green;\n            }\n            </style>\n            <div class=\"c2-container\">\n              \n            </div>\n        ";

        if (window.ShadyCSS) ShadyCSS.prepareTemplate($template, "m-child2");
        _this._$template = document.importNode($template.content, true);
        return _this;
    }

    _createClass(MChild2, [{
        key: "connectedCallback",
        value: function connectedCallback() {
            if (window.ShadyCSS) ShadyCSS.styleElement(this);
            this._root.appendChild(this._$template);
        }
    }]);

    return MChild2;
}(HTMLElement);

window.customElements.define("m-child2", MChild2);
