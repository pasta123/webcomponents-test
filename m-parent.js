class MParent extends HTMLElement {
    constructor() {
        super();
        // Shadow Root
        this._root = this.attachShadow({ mode: "open" });
        // Elements
        this._$top = null;
        this._$bottom = null;
        // Data
        this._disabled = false;
        this._value = 0;
        this._touched = false;

        let $template = document.createElement("template");
        $template.innerHTML = `
            <style>
                :host([theme="orange"]){
                  --color:gray;
                }
              .p-container{
                  position:absolute;
                  top :10px;
                  bottom: 10px;
                  left:10px;
                  right: 10px;
                  background: red;
              }
            </style>
            <div class="p-container">
              <m-child1></m-child1>
            </div>
        `;

        if (window.ShadyCSS) ShadyCSS.prepareTemplate($template, "m-parent");
        this._$template = document.importNode($template.content, true);
    }

    connectedCallback() {
        if (window.ShadyCSS) ShadyCSS.styleElement(this);
        this._root.appendChild(this._$template);
       
    }
   
    // static get observedAttributes() {
    //     return ["disabled", "value"];
    // }
    // attributeChangedCallback(name, oldValue, newValue) {
    //     if (oldValue !== newValue) {
    //         switch (name) {
    //             case "disabled":
    //                 this._disabled = (newValue !== null);
    //                 break;
    //             case "value":
    //                 if (this._touched === false) {
    //                     this._value = newValue;
    //                     this._render();
    //                 }
    //                 break;
    //         }
    //     }
    // }
}

window.customElements.define("m-parent", MParent);